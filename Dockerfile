FROM debian:buster AS builder

RUN apt-get update && apt-get install -y wget

RUN wget https://github.com/rust-lang/mdBook/releases/download/v0.3.5/mdbook-v0.3.5-x86_64-unknown-linux-gnu.tar.gz
RUN tar xf mdbook-v0.3.5-x86_64-unknown-linux-gnu.tar.gz && mv mdbook /usr/bin
RUN chmod +x /usr/bin/mdbook

FROM debian:buster

COPY --from=builder /usr/bin/mdbook /usr/bin/

CMD ["/usr/bin/mdbook"]
